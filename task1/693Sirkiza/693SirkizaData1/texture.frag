/**
 Использование текстуры в качестве коэффициента отражения света
 */

#version 330

uniform sampler2D diffuseTex1;
uniform sampler2D diffuseTex2;

struct LightConic
{
    vec3 pos;//положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
    vec3 direction;
    float cutOff;
    float outerCutOff;
    vec3 La;//цвет и интенсивность окружающего света
    vec3 Ld;//цвет и интенсивность диффузного света
    vec3 Ls;//цвет и интенсивность бликового света

    float constant;
    float linear;
    float quadratic;
};

struct LightDirect
{
    vec3 pos;//положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
    vec3 La;//цвет и интенсивность окружающего света
    vec3 Ld;//цвет и интенсивность диффузного света
    vec3 Ls;//цвет и интенсивность бликового света
};

struct LightPoint
{
    vec3 pos;//положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
    vec3 La;//цвет и интенсивность окружающего света
    vec3 Ld;//цвет и интенсивность диффузного света
    vec3 Ls;//цвет и интенсивность бликового света

    float constant;
    float linear;
    float quadratic;
};

struct LightAvialable
{
    int lightC;
    int lightD;
    int lightP;
};

uniform LightConic lightConic;
uniform LightDirect lightDirect;
uniform LightPoint lightPoint;
uniform LightAvialable lightAvailable;

in vec3 normalCamSpace;//нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace;//координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord1;//текстурные координаты (интерполирована между вершинами треугольника)
in vec2 texCoord2;//текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor;//выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0);//Коэффициент бликового отражения
const float shininess = 128.0;

vec3 CalcDirectLight(LightDirect light, vec3 diffuseColor)
{
    vec3 normal = normalize(normalCamSpace);//нормализуем нормаль после интерполяции
    vec3 viewDirection = normalize(-posCamSpace.xyz);//направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))

    vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz);//направление на источник света

    float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0);//скалярное произведение (косинус)

    vec3 color = diffuseColor * (light.La + light.Ld * NdotL);

    if (NdotL > 0.0)
    {
        vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);//биссектриса между направлениями на камеру и на источник света

        float blinnTerm = max(dot(normal, halfVector), 0.0);//интенсивность бликового освещения по Блинну
        blinnTerm = pow(blinnTerm, shininess);//регулируем размер блика
        color += light.Ls * Ks * blinnTerm;
    }

    return color;
}

vec3 CalcConicLight(LightConic light, vec3 diffuseColor)
{
    vec3 ambient  = light.La;//* attenuation;
    vec3 diffuse  = light.Ld;// * attenuation;
    vec3 specular = light.Ls;// * attenuation;

    vec3 normal = normalize(normalCamSpace);//нормализуем нормаль после интерполяции
    vec3 viewDirection = normalize(-posCamSpace.xyz);//направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))

    vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz);//направление на источник света

    float theta = dot(lightDirCamSpace, normalize(-light.direction));
    float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0);//скалярное произведение (косинус)
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);

    diffuse  *= intensity;
    specular *= intensity;

    float distance = length(light.pos - posCamSpace.xyz);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    vec3 color = diffuseColor * (ambient + diffuse * NdotL);
    if (NdotL > 0.0)
    {
        vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);//биссектриса между направлениями на камеру и на источник света

        float blinnTerm = max(dot(normal, halfVector), 0.0);//интенсивность бликового освещения по Блинну
        blinnTerm = pow(blinnTerm, shininess);//регулируем размер блика
        color += specular * Ks * blinnTerm;
    }

    return color;
}

vec3 CalcPointLight(LightPoint light, vec3 diffuseColor)
{
    vec3 ambient  = light.La;//* attenuation;
    vec3 diffuse  = light.Ld;// * attenuation;
    vec3 specular = light.Ls;// * attenuation;

    vec3 normal = normalize(normalCamSpace);//нормализуем нормаль после интерполяции
    vec3 viewDirection = normalize(-posCamSpace.xyz);//направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))

    vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz);//направление на источник света

    float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0);//скалярное произведение (косинус)

    float distance = length(light.pos - posCamSpace.xyz);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    vec3 color = diffuseColor * (ambient + diffuse * NdotL);
    if (NdotL > 0.0)
    {
        vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);//биссектриса между направлениями на камеру и на источник света

        float blinnTerm = max(dot(normal, halfVector), 0.0);//интенсивность бликового освещения по Блинну
        blinnTerm = pow(blinnTerm, shininess);//регулируем размер блика
        color += specular * Ks * blinnTerm;
    }

    return color;
}

void main()
{
    vec3 diffuseColor = texture(diffuseTex1, texCoord1).rgb * texture(diffuseTex2, texCoord2).rgb;

    if (lightAvailable.lightC == 0 && lightAvailable.lightD == 0 && lightAvailable.lightP == 0)
    {
        fragColor = vec4(diffuseColor * vec3(0.1, 0.1, 0.1), 1.0) ;
        return;
    }


    vec3 color = vec3(0.0, 0.0, 0.0);
    //color = CalcConicLight(lightConic, diffuseColor);
    if (lightAvailable.lightC != 0)
    {
        color += CalcConicLight(lightConic, diffuseColor);
    }
    if (lightAvailable.lightD != 0)
    {
        color += CalcDirectLight(lightDirect, diffuseColor);
    }
    if (lightAvailable.lightP != 0)
    {
        color += CalcPointLight(lightPoint, diffuseColor);
    }

    fragColor = vec4(color, 1.0);
}
