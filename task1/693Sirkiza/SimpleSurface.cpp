//
// Created by Evgeniy Sirkiza on 2020-03-06.
//

#include "common/Mesh.hpp"
#include "common/Application.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"
#include "common/LightInfo.hpp"
#include <glm/simd/geometric.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>

class HyperboloidApp :
        public Application
{
public:
    MeshPtr _hyperboloid;
    MeshPtr _markerDirect;
    MeshPtr _markerPoint;

    ShaderProgramPtr _hyperboloid_markerDirect_shader;
    ShaderProgramPtr _hyperboloid_markerPoint_shader;
    ShaderProgramPtr _hyperboloid_texture_shader;

    LightInfo _lightConic;
    LightInfo _lightPoint;
    LightInfo _lightDirect;
    TexturePtr _texture1;
    TexturePtr _texture2;
    GLuint _sampler1 = 0;
    GLuint _sampler2 = 0;

    float _lr = 3.0;
    float _lrSp = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    int _lightC = 1;
    int _lightP = 1;
    int _lightD = 1;

    enum class ChangeType : int
    {
        e_Increase = 1,
        e_Decrease = 2,
        e_Grid = 3
    };
public:
    HyperboloidApp() = default;

    HyperboloidApp(float hyperboloid_a, float hyperboloid_b, float hyperboloid_c, bool is_grid = false)
            : _hyperboloid_a(hyperboloid_a),
              _hyperboloid_b(hyperboloid_b),
              _hyperboloid_c(hyperboloid_c),
              _is_grid(is_grid)
    {}

public:
    void makeScene() override
    {
        Application::makeScene();

        float time = glfwGetTime();

        _cameraMover = std::make_shared<OrbitCameraMover>();

        _markerDirect = makeSphere(0.1f);
        _markerPoint = makeSphere(0.1f);
        _hyperboloid = buildHyperboloid();

        _hyperboloid->setModelMatrix(glm::mat4(1.0f));
        //_hyperboloid->setModelMatrix(glm::translate(glm::mat4(0.4f), glm::vec3(0.0f, 0.2f, 0.3f)) *
        // glm::rotate(-2.55f, glm::vec3(6.0f, -0.6f, 0.0f)));

        _hyperboloid_markerDirect_shader = std::make_shared<ShaderProgram>("693SirkizaData1/marker.vert",
                                                                     "693SirkizaData1/marker.frag");

        _hyperboloid_markerPoint_shader = std::make_shared<ShaderProgram>("693SirkizaData1/marker.vert",
                                                                     "693SirkizaData1/marker.frag");

        _hyperboloid_texture_shader = std::make_shared<ShaderProgram>("693SirkizaData1/texture.vert",
                                                                      "693SirkizaData1/texture.frag");

        _lightConic.position = glm::vec3(0.0, 0.0, 0.0);
        _lightDirect.position =
                glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _lightPoint.position = glm::vec3(0.5 * cos(time), 0.5 * sin(time), 0) * _lrSp;

        _lightPoint.ambient = _lightDirect.ambient = _lightConic.ambient = glm::vec3(0.1, 0.1, 0.1);
        _lightPoint.diffuse = _lightDirect.diffuse = _lightConic.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _lightPoint.specular = _lightDirect.specular = _lightConic.specular = glm::vec3(1.0, 1.0, 1.0);
        _texture1 = loadTexture("693SirkizaData1/texture1.jpg");
        _texture2 = loadTexture("693SirkizaData1/texture2.jpg");

        glGenSamplers(1, &_sampler1);
        glSamplerParameteri(_sampler1, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler1, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_sampler2);
        glSamplerParameteri(_sampler2, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler2, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void draw() override
    {
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _hyperboloid_texture_shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _hyperboloid_texture_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _hyperboloid_texture_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        float time = glfwGetTime();

        _lightConic.position = glm::vec3(0.0, 0.0, 0.0); //_cameraMover->returnPosition();
        _lightDirect.position =
                glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _lightPoint.position = glm::vec3(0.5 * cos(time), 0.5 * sin(time), 0) * _lrSp;

        //glm::vec3 lightPosCamSpace = glm::vec3(0.0, 0.0,
        //                                       0.0);//glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _hyperboloid_texture_shader->setVec3Uniform("lightConic.pos",
                                                    glm::vec3(0.0, 0.0,
                                                              0.0)); //копируем положение уже в системе виртуальной камеры
        _hyperboloid_texture_shader->setVec3Uniform("lightConic.direction", glm::vec3(0.0, 0.0, -1.0));
        _hyperboloid_texture_shader->setFloatUniform("lightConic.cutOff", cos(glm::radians(8.5f)));
        _hyperboloid_texture_shader->setFloatUniform("lightConic.outerCutOff", glm::cos(glm::radians(10.5f)));
        _hyperboloid_texture_shader->setVec3Uniform("lightConic.La", _lightConic.ambient);
        _hyperboloid_texture_shader->setVec3Uniform("lightConic.Ld", _lightConic.diffuse);
        _hyperboloid_texture_shader->setVec3Uniform("lightConic.Ls", _lightConic.specular);
        _hyperboloid_texture_shader->setFloatUniform("lightConic.constant", 1.0f);
        _hyperboloid_texture_shader->setFloatUniform("lightConic.linear", 0.09f);
        _hyperboloid_texture_shader->setFloatUniform("lightConic.quadratic", 0.037f);


        _hyperboloid_texture_shader->setVec3Uniform("lightDirect.pos",
                                                    glm::vec3(_camera.viewMatrix *
                                                              glm::vec4(_lightDirect.position,
                                                                        1.0))); //копируем положение уже в системе виртуальной камеры
        _hyperboloid_texture_shader->setVec3Uniform("lightDirect.La", _lightDirect.ambient);
        _hyperboloid_texture_shader->setVec3Uniform("lightDirect.Ld", _lightDirect.diffuse);
        _hyperboloid_texture_shader->setVec3Uniform("lightDirect.Ls", _lightDirect.specular);

        _hyperboloid_texture_shader->setVec3Uniform("lightPoint.pos",
                                                    glm::vec3(_camera.viewMatrix *
                                                              glm::vec4(_lightPoint.position,
                                                                        1.0))); //копируем положение уже в системе виртуальной камеры
        _hyperboloid_texture_shader->setVec3Uniform("lightPoint.La", _lightPoint.ambient);
        _hyperboloid_texture_shader->setVec3Uniform("lightPoint.Ld", _lightPoint.diffuse);
        _hyperboloid_texture_shader->setVec3Uniform("lightPoint.Ls", _lightPoint.specular);
        _hyperboloid_texture_shader->setFloatUniform("lightPoint.constant", 1.0f);
        _hyperboloid_texture_shader->setFloatUniform("lightPoint.linear", 0.1f);
        _hyperboloid_texture_shader->setFloatUniform("lightPoint.quadratic", 0.039f);

        _hyperboloid_texture_shader->setIntUniform("lightAvailable.lightC", _lightC);
        _hyperboloid_texture_shader->setIntUniform("lightAvailable.lightD", _lightD);
        _hyperboloid_texture_shader->setIntUniform("lightAvailable.lightP", _lightP);


        _hyperboloid_texture_shader->setFloatUniform("time", time / 10);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler1);
        _texture1->bind();
        _hyperboloid_texture_shader->setIntUniform("diffuseTex1", 0);

        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _sampler2);
        _texture2->bind();
        _hyperboloid_texture_shader->setIntUniform("diffuseTex2", 1);


        _hyperboloid_texture_shader->setMat4Uniform("modelMatrix", _hyperboloid->modelMatrix());
        _hyperboloid_texture_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(
                glm::inverse(glm::mat3(_camera.viewMatrix * _hyperboloid->modelMatrix()))));

        if (_is_grid)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }

        _hyperboloid->draw();

        if (_is_grid)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }

        _hyperboloid_markerDirect_shader->use();
        _hyperboloid_markerDirect_shader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix *
                                                                glm::translate(glm::mat4(0.5f), _lightDirect.position));
        _hyperboloid_markerDirect_shader->setVec4Uniform("color", glm::vec4(_lightDirect.diffuse, 1.0f));
        _markerDirect->draw();

        _hyperboloid_markerPoint_shader->use();
        _hyperboloid_markerPoint_shader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix *
                                                                glm::translate(glm::mat4(0.5f), _lightPoint.position));
        _hyperboloid_markerPoint_shader->setVec4Uniform("color", glm::vec4(_lightPoint.diffuse, 1.0f));
        _markerPoint->draw();

        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Hyperboloid", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_lightDirect.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_lightDirect.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_lightDirect.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());

                ImGui::SliderFloat("SpininnigRadius", &_lrSp, 0.1f, 10.0f);
                ImGui::SliderInt("PointLighter", &_lightP, 0, 1);
                ImGui::SliderInt("DirectLighter", &_lightD, 0, 1);
                ImGui::SliderInt("ConicLighter", &_lightC, 0, 1);
            }
        }
        ImGui::End();
    }

    void updateFigure(const ChangeType& type)
    {
        if (type == ChangeType::e_Increase)
            _det += _det_diff;
        else if (type == ChangeType::e_Decrease)
            _det = _det > _det_diff ? _det - _det_diff : 0;
        else if (type == ChangeType::e_Grid)
            _is_grid = !_is_grid;

        _hyperboloid = buildHyperboloid();
        _hyperboloid->setModelMatrix(glm::mat4(1.0f));

        //_hyperboloid->setModelMatrix(glm::translate(glm::mat4(0.4f), glm::vec3(0.0f, 0.2f, 0.3f)) *
        //                             glm::rotate(-2.55f, glm::vec3(6.0f, -0.6f, 0.0f)));
        draw();
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_EQUAL)
            {
                updateFigure(ChangeType::e_Increase);
            } else if (key == GLFW_KEY_MINUS)
            {
                updateFigure(ChangeType::e_Decrease);
            } else if (key == GLFW_KEY_G)
            {
                updateFigure(ChangeType::e_Grid);
            }
        }
    }

private:
    MeshPtr buildHyperboloid()
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> texcoords1;
        std::vector<glm::vec2> texcoords2;

        int delta = _det / 10;

        if (_det > 0)
        {
            float piece_v = _v_size / _det;
            float piece_theta = _theta_size / _det;

            for (int i = -static_cast<int>(_det); i < static_cast<int>(_det); ++i)
            {
                float v1 = piece_v * i;
                float v2 = v1 + piece_v;

                for (int j = 0; j < _det; ++j)
                {
                    float theta1 = piece_theta * j;
                    float theta2 = theta1 + piece_theta;

                    glm::vec3 a = hyperboloidPoint(v1, theta1);
                    glm::vec3 b = hyperboloidPoint(v1, theta2);
                    glm::vec3 c = hyperboloidPoint(v2, theta2);
                    glm::vec3 d = hyperboloidPoint(v2, theta1);

                    // First triangle
                    vertices.insert(vertices.end(), {a, c, d});

                    // Second triangle
                    vertices.insert(vertices.end(), {a, b, c});

                    // Normal
                    normals.push_back(hyperboloidNormal(v1, theta1));
                    normals.push_back(hyperboloidNormal(v2, theta2));
                    normals.push_back(hyperboloidNormal(v2, theta1));
                    normals.push_back(hyperboloidNormal(v1, theta1));
                    normals.push_back(hyperboloidNormal(v1, theta2));
                    normals.push_back(hyperboloidNormal(v2, theta2));

                    //normals.insert(normals.end(), 3, -normalize(glm::cross(b - a, c - a)));
                    //normals.insert(normals.end(), 3, -normalize(glm::cross(d - c, a - c)));

                    // Texture

                    texcoords1.emplace_back((float) j / _det, 1.0f - (float) (i + _det) / (2 * _det));
                    texcoords1.emplace_back((float) (j + 1) / _det, 1.0f - (float) (i + _det + 1) / (2 * _det));
                    texcoords1.emplace_back((float) (j + 1) / _det, 1.0f - (float) (i + _det) / (2 * _det));
                    texcoords1.emplace_back((float) j / _det, 1.0f - (float) (i + _det) / (2 * _det));
                    texcoords1.emplace_back((float) (j) / _det, 1.0f - (float) (i + _det + 1) / (2 * _det));
                    texcoords1.emplace_back((float) (j + 1) / _det, 1.0f - (float) (i + _det + 1) / (2 * _det));

                    texcoords2.emplace_back(1.0f - (float) j / _det, (float) (i + _det) / (2 * _det));
                    texcoords2.emplace_back(1.0f - (float) (j + 1) / _det, (float) (i + _det + 1) / (2 * _det));
                    texcoords2.emplace_back(1.0f - (float) (j + 1) / _det, (float) (i + _det) / (2 * _det));
                    texcoords2.emplace_back(1.0f - (float) j / _det, (float) (i + _det) / (2 * _det));
                    texcoords2.emplace_back(1.0f - (float) (j) / _det, (float) (i + _det + 1) / (2 * _det));
                    texcoords2.emplace_back(1.0f - (float) (j + 1) / _det, (float) (i + _det + 1) / (2 * _det));

                }

            }
        }

        auto mesh = std::make_shared<Mesh>();

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(texcoords1.size() * sizeof(float) * 2, texcoords1.data());

        DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf3->setData(texcoords2.size() * sizeof(float) * 2, texcoords2.data());

        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, buf3);

        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        return mesh;
    }

    glm::vec3 hyperboloidPoint(float v, float theta)
    {
        return glm::vec3(_hyperboloid_a * cosh(v) * cos(theta),
                         _hyperboloid_b * cosh(v) * sin(theta),
                         _hyperboloid_c * sinh(v));
    }

    glm::vec3 hyperboloidNormal(float v, float theta)
    {
        return glm::vec3(
                _hyperboloid_c * _hyperboloid_b * cosh(v) * cos(theta),
                _hyperboloid_c * _hyperboloid_a * cosh(v) * sin(theta),
                -_hyperboloid_a * sinh(v) * _hyperboloid_b
        );
    }

private:
    unsigned int _det = 400;
    float _hyperboloid_a = 0.5f;
    float _hyperboloid_b = 0.5f;
    float _hyperboloid_c = 0.5f;
    bool _is_grid = false;
    const float _v_size = 1.5f;
    const float _theta_size = 2 * glm::pi<float>();
    const unsigned int _det_diff = 3;
};

int main()
{
    HyperboloidApp app(0.5f, 0.4f, 0.6f);
    app.start();
    return 0;
}
